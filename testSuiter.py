import signUp_test
import unittest
from selenium import webdriver
from pages.signUpPage import signUpPage
from pages.vardata import name, email, password, passwordConfirm
from pages.base_page import BasePage
import unittest

suite = unittest.TestLoader().loadTestsFromModule(signUp_test)
unittest.TextTestRunner(verbosity=1).run(suite)
if __name__ == "__main__":
    unittest.main()