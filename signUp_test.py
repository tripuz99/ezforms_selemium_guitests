from selenium import webdriver
from pages.signUpPage import signUpPage
from pages.vardata import name, email, password, passwordConfirm
from pages.base_page import BasePage
import unittest

class signUpTest(unittest.TestCase, BasePage):
    def test_signup(self):
        #test setup
        driver = webdriver.Chrome()
        regpage = signUpPage(driver)
        driver.maximize_window()
        #test execution
        regpage.go()
        regpage.nameInput.input_text(name)
        regpage.emailInput.input_text(email)
        regpage.passwordConfirmInput.input_text(passwordConfirm)
        regpage.passwordInput.input_text(password)
        regpage.registerButton.click()
        if "You are logged in!" in driver.page_source:
            print("Success signup")
        elif "The email has already been taken." in driver.page_source:
            print ("The email has already been taken")
if __name__ == "__main__":
    unittest.main()