from selenium import webdriver
from pages.passwordResetLinkPage import passwordResetLinkPage
from pages.passwordResetPage import passwordResetPage
from pages.vardata import name, email, password, passwordConfirm
from pages.base_page import BasePage
import unittest

class resetPasswordTest(unittest.TestCase, BasePage):
  def test_password_reset(self):
        #test setup
        driver = webdriver.Chrome()
        reslinkpass = passwordResetLinkPage(driver)
        respasspage = passwordResetPage(driver)
        driver.maximize_window()
        #test execution
        reslinkpass.go()
        reslinkpass.emailInput.input_text(email)
        reslinkpass.sendPasswordLink.click()
        ##Should be reworked after uploading to PRODUCTION

if __name__ == "__main__":
    unittest.main()