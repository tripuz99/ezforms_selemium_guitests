from selenium.webdriver.common.by import By
from pages.base_element import BaseElement
from pages.base_page import BasePage
from pages.locator import Locator
from pages.vardata import passwordResetUrl

class passwordResetPage(BasePage):
    @property
    def passwordInput(self):
        locator = Locator(by=By.ID, value='password')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def passwordConfirmInput(self):
        locator = Locator(by=By.ID, value='password-confirm')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def resetPasswordButton(self):
        locator = Locator(by=By.XPATH, value="//button[@type='submit']")
        return BaseElement(
            driver=self.driver,
            locator=locator
        )

