from selenium.webdriver.common.by import By
from pages.base_element import BaseElement
from pages.base_page import BasePage
from pages.locator import Locator
from pages.vardata import baseUrl

class logInPage(BasePage):
    url = baseUrl
    @property
    def emailInput(self):
        locator = Locator(by=By.ID, value='email')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def passwordInput(self):
        locator = Locator(by=By.ID, value='password')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def rememberMeInput(self): #checkbox
        locator = Locator(by=By.ID, value='remember')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def forgotPasswordButton(self): 
        locator = Locator(by=By.XPATH, value="//a[contains(text(),'Forgot your Password?')]")
        return BaseElement(
            driver=self.driver,
            locator=locator
    )
    @property
    def signInButton(self): 
        locator = Locator(by=By.XPATH, value="//input[@type='submit']")
        return BaseElement(
            driver=self.driver,
            locator=locator
    )

    



