from selenium.webdriver.common.by import By
from pages.base_element import BaseElement
from pages.base_page import BasePage
from pages.locator import Locator
from pages.vardata import passwordResetUrl

class passwordResetLinkPage(BasePage):

    url = passwordResetUrl

    @property
    def emailInput(self):
        locator = Locator(by=By.ID, value='email')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def sendPasswordLink(self):
        locator = Locator (by=By.XPATH, value="//input[@type='submit']")
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def logInPageButton(self):
        locator = Locator (by=By.PARTIAL_LINK_TEXT, value="/login']")
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    def signUpPageButton(self):
        locator = Locator (by=By.PARTIAL_LINK_TEXT, value="/register']")
        return BaseElement(
            driver=self.driver,
            locator=locator
        )