from selenium.webdriver.common.by import By
from pages.base_element import BaseElement
from pages.base_page import BasePage
from pages.locator import Locator
from pages.vardata import signUpUrl

class signUpPage(BasePage):
    
    url = signUpUrl

    @property
    def nameInput(self):
        locator = Locator(by=By.ID, value='name')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def emailInput(self):
        locator = Locator(by=By.ID, value='email')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def passwordInput(self):
        locator = Locator(by=By.ID, value='password')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def passwordConfirmInput(self):
        locator = Locator(by=By.ID, value='password-confirm')
        return BaseElement(
            driver=self.driver,
            locator=locator
        )
    @property
    def registerButton(self):
        locator = Locator(by=By.XPATH, value="//input[@type='submit']")
        return BaseElement(
            driver=self.driver,
            locator=locator
        )