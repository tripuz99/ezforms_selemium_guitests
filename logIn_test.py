from selenium import webdriver
from pages.loginPage import logInPage
from pages.vardata import email, password
from pages.base_page import BasePage
import unittest
import time

class loginTest(unittest.TestCase, BasePage):
    def test_logIn(self):
        #test setup
        driver = webdriver.Chrome()
        logpage = logInPage(driver)
        driver.maximize_window()
        #test execution
        logpage.go()
        logpage.emailInput.input_text(email)
        logpage.passwordInput.input_text(password)
        logpage.rememberMeInput.click()
        logpage.signInButton.click()
        if "You are logged in!" in driver.page_source:
            print("Success login")
        else:
            print("Something wrong!!!")
if __name__ == "__main__":
    unittest.main()       

        
        
